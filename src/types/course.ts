import { ListItemInterface } from "../components/CustomList/CustomList";

export interface Course extends  ListItemInterface{
    id: string | number,
    nombre: string,
    sigla: string,
    estado?: boolean
 }