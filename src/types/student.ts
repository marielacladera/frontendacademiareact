import { ListItemInterface } from "../components/CustomList/CustomList";

export interface Student extends ListItemInterface {
    id: number | string;
    nombres: string;
    apellidos: string;
    dni: string;
    edad: number;
}