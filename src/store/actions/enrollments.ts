import { createAction } from '@reduxjs/toolkit';
import httpClient from '../../services/httpClient';
import {
    studentsUrl,
    enrollmentsByUserUrl,
    enrollmentsUrl,
    coursesUrl,
} from '../../constants/servicesUrls';

export enum EnrollmentsActionTypes {
    GET_ENROLLMENTS_BY_USER_SUCCESS = 'GET_ENROLLMENTS_BY_USER_SUCCESS',
    GET_ENROLLMENTS_BY_USER_FAIL = 'GET_ENROLLMENTS_BY_USER_FAIL',
    TOGGLE_GET_ENROLLMENTS_BY_USER_STATE = 'TOGGLE_GET_ENROLLMENTS_BY_USER_STATE',
    GET_ENROLLMENTS_DETAIL_SUCCESS = 'GET_ENROLLMENTS_DETAIL_SUCCESS',
    GET_ENROLLMENTS_DETAIL_FAIL = 'GET_ENROLLMENTS_DETAIL_FAIL',
    TOGGLE_GET_ENROLLMENTS_DETAIL = 'TOGGLE_GET_ENROLLMENTS_DETAIL',
    GET_ENROLLMENTS_SUCCESS = 'GET_ENROLLMENTS_SUCCESS',
    GET_ENROLLMENTS_FAIL = 'GET_ENROLLMENTS_FAIL',
    TOGGLE_GET_ENROLLMENTS_STATE = 'TOGGLE_GET_ENROLLMENTS_STATE',
    CLEAR_ENROLLMENTS_BY_USER = 'CLEAR_ENROLLMENTS_BY_USER',
}

export const getEnrollmentsByUserSuccess = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_BY_USER_SUCCESS
);
export const getEnrollmentsByUserFail = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_BY_USER_FAIL
);
export const toggleGetEnrollmentsByUserState = createAction<boolean>(
    EnrollmentsActionTypes.TOGGLE_GET_ENROLLMENTS_BY_USER_STATE
);

export const getEnrollmentsDetailSuccess = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_DETAIL_SUCCESS
);
export const getEnrollmentsDetailFail = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_DETAIL_FAIL
);
export const toggleGetEnrollmentsDetailState = createAction<boolean>(
    EnrollmentsActionTypes.TOGGLE_GET_ENROLLMENTS_DETAIL
);

export const getEnrollmentsSuccess = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_SUCCESS
);
export const getEnrollmentsFail = createAction<any>(
    EnrollmentsActionTypes.GET_ENROLLMENTS_FAIL
);
export const toggleGetEnrollmentsState = createAction<boolean>(
    EnrollmentsActionTypes.TOGGLE_GET_ENROLLMENTS_STATE
);

export const clearEnrollmentsByUser = createAction<undefined>(
    EnrollmentsActionTypes.CLEAR_ENROLLMENTS_BY_USER
);

export const getEnrollmentsByUser = (userId: any) => async (dispatch: any) => {
    dispatch(toggleGetEnrollmentsByUserState(true));
    try {
        const payload = {
            idCliente: userId,
        };
        const response = await httpClient.post(enrollmentsUrl, payload);

        dispatch(getEnrollmentsByUserSuccess(response.data));
    } catch (e) {
        dispatch(getEnrollmentsByUserFail(e));
    }

    dispatch(toggleGetEnrollmentsByUserState(false));
};

export const getInvoiceDetail =
    (invoiceId: any, callback: any) => async (dispatch: any) => {
        dispatch(toggleGetEnrollmentsDetailState(true));
        try {
            const invoiceResponse = await httpClient.get(
                `${enrollmentsUrl}/${invoiceId}`
            );

            const clientId = invoiceResponse.data.cliente.id;
            const responseClient = await httpClient.get(
                `${studentsUrl}/${clientId}`
            );

            const platesRequest = invoiceResponse.data.items.map((p: any) => {
                return httpClient
                    .get(`${coursesUrl}/${p.plato.id}`)
                    .then(({ data }: any) => data);
            });

            const platesResponse = await Promise.all(platesRequest);

            const jsonResponse = {
                invoice: invoiceResponse.data,
                client: responseClient.data,
                plates: platesResponse,
            };

            dispatch(getEnrollmentsDetailSuccess(jsonResponse));
            callback();
        } catch (error) {
            dispatch(getEnrollmentsDetailFail(error));
        }

        dispatch(toggleGetEnrollmentsDetailState(false));
    };

export const getEnrollmentsList = () => async (dispatch: any) => {
    dispatch(toggleGetEnrollmentsState(true));
    try {
        const response = await httpClient.get(enrollmentsUrl);

        dispatch(getEnrollmentsSuccess(response.data));
    } catch (error) {
        dispatch(getEnrollmentsFail(error));
    }

    dispatch(dispatch(toggleGetEnrollmentsState(false)));
};
