import { createReducer } from '@reduxjs/toolkit'
import {
    clearEnrollmentsByUser,
    getEnrollmentsByUserFail,
    getEnrollmentsByUserSuccess,
    getEnrollmentsDetailFail,
    getEnrollmentsDetailSuccess,
    getEnrollmentsFail,
    getEnrollmentsSuccess,
    toggleGetEnrollmentsByUserState,
    toggleGetEnrollmentsDetailState,
    toggleGetEnrollmentsState,
} from '../actions/enrollments'

interface InnerState {
    loading: boolean
    data: any
    error: { message: string } | null
}

export interface EnrollmentsState {
    list: InnerState
    filterList: InnerState
    enrollmentDetail: InnerState
}

const getInitialInnerState = () => {
    return {
        loading: false,
        data: null,
        error: null,
    }
}

const getInitialState = () => {
    return {
        list: getInitialInnerState(),
        filterList: getInitialInnerState(),
        enrollmentDetail: getInitialInnerState(),
    }
}

export const getEnrollmentsByUserSuccessExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    state.filterList.data = action.payload
}

export const getEnrollmentsByUserFailExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    const { message } = action.payload
    state.filterList.error = { message }
}

export const toggleGetEnrollmentsByUserStateExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    state.filterList.loading = action.payload
}

export const clearEnrollmentsByUserExecutor = (state: EnrollmentsState) => {
    return {
        ...state,
        filterList: getInitialInnerState(),
    }
}
export const getEnrollmentsDetailSuccessExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    state.enrollmentDetail.data = action.payload
}
export const getEnrollmentsDetailFailExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    const { message } = action.payload
    state.enrollmentDetail.error = { message }
}
export const toggleGetEnrollmentsDetailStateExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    state.enrollmentDetail.loading = action.payload
}
export const getEnrollmentsSuccessExecutor = (state: EnrollmentsState, action: any) => {
    state.list.data = action.payload
}
export const getEnrollmentsExecutor = (state: EnrollmentsState, action: any) => {
    const { message } = action.payload
    state.list.error = { message }
}

export const toggleGetEnrollmentsStateExecutor = (
    state: EnrollmentsState,
    action: any
) => {
    state.list.loading = action.payload
}

const enrollmentReducerBuilder = (builder: any) => {
    return builder
        .addCase(getEnrollmentsByUserSuccess, getEnrollmentsByUserSuccessExecutor)
        .addCase(getEnrollmentsByUserFail, getEnrollmentsByUserFailExecutor)
        .addCase(
            toggleGetEnrollmentsByUserState,
            toggleGetEnrollmentsByUserStateExecutor
        )
        .addCase(getEnrollmentsDetailSuccess, getEnrollmentsDetailSuccessExecutor)
        .addCase(getEnrollmentsDetailFail, getEnrollmentsDetailFailExecutor)
        .addCase(
            toggleGetEnrollmentsDetailState,
            toggleGetEnrollmentsDetailStateExecutor
        )
        .addCase(getEnrollmentsSuccess, getEnrollmentsSuccessExecutor)
        .addCase(getEnrollmentsFail, getEnrollmentsExecutor)
        .addCase(toggleGetEnrollmentsState, toggleGetEnrollmentsStateExecutor)
        .addCase(clearEnrollmentsByUser, clearEnrollmentsByUserExecutor)
}

export const enrollmentsReducer = createReducer(
    getInitialState(),
    enrollmentReducerBuilder
)
