import { createSelector } from "@reduxjs/toolkit";
import { SessionState } from "../reducers/session";

export const sessionStateSelector = (state: any): SessionState => state.session;

export const sessionAuthenticatedSelector = createSelector (
    sessionStateSelector, 
    (sessionState) => sessionState.authenticated
);

export const sessionAuthenticatedInProgressSelector = createSelector (
    sessionStateSelector,
    (sessionState) => sessionState.authenticationInProgress
);

export const sessionAuthenticatedError = createSelector (
    sessionStateSelector,
    (sesionState) => sesionState.authenticationError
);
