import { createSelector } from '@reduxjs/toolkit';
import { StudentsState } from '../reducers/students';

export const studentsStateSelector = (state: any): StudentsState => state.students;

export const studentListSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.studentsList;
    }
);

export const getStudentsListInProgressSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.getStudentsListInProgress;
    }
);

export const getStudentsListErrorSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.getStudentsListError;
    }
);

export const updateStudentInProgressSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.updateStudentInProgress;
    }
);

export const updateStudentErrorSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.updateStudentError;
    }
);

export const deleteStudentInProgressSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.deleteStudentInProgress;
    }
);

export const createStudentInProgressSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.createStudentInProgress;
    }
);

export const createStudentErrorSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.createStudentError;
    }
);

export const deleteStudentErrorSelector = createSelector(
    studentsStateSelector,
    (studentsState) => {
        return studentsState.deleteStudentError;
    }
);