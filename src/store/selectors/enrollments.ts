import { createSelector } from "@reduxjs/toolkit";
import { EnrollmentsState } from "../reducers/enrollments";

export const enrollmentStateSelector = (store:any): EnrollmentsState => store.enrollments;
export const invoiceDetailSelector = createSelector(
  enrollmentStateSelector,
  (enrollmentState) => {
    return enrollmentState.enrollmentDetail.data;
  }
);
