import { configureStore } from '@reduxjs/toolkit';
import { sessionReducer as session } from './reducers/session';
import { coursesReducer as courses } from './reducers/courses';
import { studentsReducer as students } from './reducers/students';
import { enrollmentsReducer as enrollments } from './reducers/enrollments';

export const store = configureStore ({
    reducer:{ session, courses, students, enrollments},
});