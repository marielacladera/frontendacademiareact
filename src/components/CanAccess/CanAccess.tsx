import React from "react";
import { Navigate } from "react-router";
import { loginRoute } from "../../constants/routes";
import { getToken } from "../../utils/tokenManagement";

const CanAccess = (
    ComponentToRender: React.FunctionComponent | React.ComponentClass
) => {
    const token = getToken();
    return (props: any) => {
        if (token) {
            return <ComponentToRender {...props}/>
        }
        return <Navigate to={loginRoute} />
    };
};

export default CanAccess;