import { List } from "@mui/material";

export interface ListItemInterface {
    id: number | string
};

export interface ListItemComponent<T extends ListItemInterface> {
    item: T,
    onUpdate: (item: T) => void,
    onDelete: (item: T) => void
};

export interface ListProps<T extends ListItemInterface> {
    collection: T[],
    renderAs: React.FC<ListItemComponent<T>> 
    onUpdate: (item: T) => void,
    onDelete: (item: T) => void
};

const CustomList = <T extends ListItemInterface> (props: ListProps<T>) => {
    const {
        collection, 
        renderAs, 
        onUpdate, 
        onDelete
    } = props
    return(
        <List>
            {
                collection && 
                collection.map(function createListItem(item){
                    return renderAs({item, onUpdate, onDelete})
                })
            }
        </List>
    )
};

export default CustomList;