import { Delete, Edit, MenuBook } from "@mui/icons-material";
import { Avatar, IconButton, ListItem, ListItemSecondaryAction, ListItemText } from "@mui/material";
import { Course } from "../../../types/course";
import { ListItemComponent } from "../../CustomList/CustomList";

const CourseListItem: React.FC<ListItemComponent<Course>> = ({
    item,
    onDelete,
    onUpdate
}) => {
    return (
        <ListItem key={item.id}>
            <Avatar>
                <MenuBook />
            </Avatar>
            <ListItemText
                primary={item.nombre}
                secondary={`Initials: ${item.sigla}`}
            />

            <ListItemSecondaryAction>
                <IconButton
                    onClick={() => onUpdate(item)}
                >
                    <Edit />
                </IconButton>

                <IconButton
                    onClick={() => onDelete(item)}
                >
                    <Delete />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}

export default CourseListItem;