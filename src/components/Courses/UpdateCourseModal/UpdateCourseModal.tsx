import { Save } from "@mui/icons-material";
import { TextField } from "@mui/material";
import {  useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CoursesActions } from "../../../store/actions/courses";
import { createCourseInProgressSelector, deleteCourseInProgress, updateCourseInProgressSelector } from "../../../store/selectors/courses";
import { Course } from "../../../types/course";
import Modal from "../../Modal";

interface UpdateCourseModalProps{
   course?:  Course,
   open: boolean,
   onClose: () => void
}

const UpdateCourseModal: React.FC<UpdateCourseModalProps> = ({
    course,
    open,
    onClose
}) => {
    const [name, setName] = useState('');
    const [initials, setInitials] = useState('');
    const [dirty, setDirty] = useState(false);
    const updateInProgress = useSelector(updateCourseInProgressSelector);
    const createInProgres =useSelector(createCourseInProgressSelector);
    const create = !course;
    const confirmLabel = create ? 'Create' : 'Update';
    const title = `${confirmLabel} Course`
    const dispatch = useDispatch();

    useEffect(() => {
        if(!dirty) return
        if(!(updateInProgress || createInProgres)){
            onClose()
            setDirty(false)
            setName('')
            setInitials('')
        }
    }, [createInProgres, dirty, onclose, updateInProgress]);

    useEffect(() => {
        if(!course){
            setName('')
            setInitials('')
            return
        }
        setName(course.nombre)
        setInitials(course.sigla)
    }, [course])

    const handleConfirm = (
        payload: Partial<Course>, 
        currentCourse: Course | undefined
    ) => {
        const payloadCandidate: Course = !create
            ? ({...currentCourse, ...payload} as Course) //object.assign({}, currentCourse, payload)
            : ({...payload, estado: true} as Course);
        const dispatchAction = !create
            ?CoursesActions.updateCourse
            : CoursesActions.createCourse;

        dispatch(dispatchAction(payloadCandidate));
        setDirty(true)
    }

    return (
        <Modal
            title = {title}
            isOpen = {open}
            onClose = {onClose}
            confirmLabel = {confirmLabel}
            confirmIcon = {<Save/>}
            updateInProgress= {updateInProgress || createInProgres}
            onConfirm = {() => 
                handleConfirm({nombre: name, sigla: initials}, course)
            }
        >
            <TextField 
                label="Name"
                type="text" 
                fullWidth 
                margin= "dense" 
                value={name}
                onChange={(e) => setName(e.target.value)}
            />
            <TextField 
                label="Initials"
                type="text" 
                fullWidth 
                margin="dense" 
                value={initials}
                onChange={(e) => setInitials(e.target.value)}
            />
        </Modal>
    )
}

export default UpdateCourseModal;