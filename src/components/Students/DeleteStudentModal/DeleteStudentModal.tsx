import Modal from '../../Modal/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { Delete } from '@mui/icons-material';
import { useEffect, useState } from 'react';
import { Student } from '../../../types/student';
import { deleteStudentInProgressSelector } from '../../../store/selectors/students';
import { StudentsActions } from '../../../store/actions/student';

interface DeleteStudentModalProps {
    student: Student;
    open: boolean;
    onClose: () => void;
}

const DeleteStudentModal: React.FC<DeleteStudentModalProps> = ({
    open,
    student,
    onClose,
}) => {
    const dispatch = useDispatch();
    const [commited, setCommited] = useState(false);
    const deleteInProgress = useSelector(deleteStudentInProgressSelector);

    useEffect(() => {
        if (!commited) return;

        if (!deleteInProgress) {
            onClose();
            setCommited(false);
        }
    }, [commited, deleteInProgress, onClose]);
    const dismiss = () => {
        onClose();
    };

    const handleOnClose = () => {
        dismiss();
    };
    const handleOnConfirm = () => {
        dispatch(StudentsActions.deleteStudent(student.id));
        setCommited(true);
    };
    return (
        <Modal
            onClose={handleOnClose}
            isOpen={open}
            title="Delete student"
            onConfirm={handleOnConfirm}
            updateInProgress={deleteInProgress}
            confirmLabel="Delete"
            confirmIcon={<Delete />}
        >
            <span>
                Are you sure you want to delete{' '}
                {`${student?.nombres} ${student?.apellidos}`}?
            </span>
        </Modal>
    );
};

export default DeleteStudentModal;
