import React from 'react';
import { ListItemComponent } from '../../CustomList/CustomList';
import {
    Avatar,
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from '@mui/material';
import { Delete, Edit, EmojiPeopleOutlined } from '@mui/icons-material';
import { Student } from '../../../types/student';

const StudentListItem: React.FC<ListItemComponent<Student>> = ({
    item,
    onUpdate,
    onDelete,
}) => {
    return (
        <ListItem key={item.id}>
            <Avatar>
                <EmojiPeopleOutlined />
            </Avatar>
            <ListItemText
                primary={`${item.nombres} ${item.apellidos}`}
                secondary={`Dni: ${item.dni}`}
            />

            <ListItemSecondaryAction>
                <IconButton onClick={() => onUpdate(item)}>
                    <Edit />
                </IconButton>

                <IconButton onClick={() => onDelete(item)}>
                    <Delete />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
};

export default StudentListItem;
