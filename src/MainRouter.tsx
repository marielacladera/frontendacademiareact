import { useDispatch } from "react-redux"
import { BrowserRouter,Route, Routes } from "react-router-dom"
import PrivateRoute from "./components/PrivateRoute"
import { baseRoute, coursesRoute, enrollmentsRoute, loginRoute, studentsRoute } from "./constants/routes"
import Courses from "./pages/Courses"
import Enrollments from "./pages/Enrollments"
import EnrollmentsCreate from "./pages/EnrollmentsCreate"
import Login from "./pages/Login"
import Students from "./pages/Students"
import { SessionActions } from "./store/actions/session"
import { decodeToken, getToken } from "./utils/tokenManagement"

const MainRouter = () => {
    const token = getToken();
    const dispatch = useDispatch();
    if(token) {
        const decodeUserData = decodeToken();
        dispatch(SessionActions.onLoginSuccess(decodeUserData));
    }
    //const SecuredCourses = CanAccess(Courses);
    return (
        <BrowserRouter>
            <Routes>
                <Route path = { baseRoute } element = { <Login/> } />
                <Route path = { loginRoute } element = { <Login/> } />
                <Route path = { coursesRoute } element = { <PrivateRoute><Courses /></PrivateRoute> } />
                <Route path = { studentsRoute } element = { <PrivateRoute><Students /></PrivateRoute> } />
                <Route path = { `${enrollmentsRoute}/new` } element = {<PrivateRoute><EnrollmentsCreate /></PrivateRoute>} />
                <Route path = { enrollmentsRoute } element = {<PrivateRoute><Enrollments /></PrivateRoute>} />
                <Route path = "*" element = { <span>404</span >} />
            </Routes>
        </BrowserRouter>
    );
};


export default MainRouter;
