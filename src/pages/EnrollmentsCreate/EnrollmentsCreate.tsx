import { DatePicker } from "@mui/lab";
import { Grid, IconButton, Select, TextField, MenuItem, Button, Paper } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Layout from "../../components/Layout";
import { CoursesActions } from "../../store/actions/courses";
import { StudentsActions } from "../../store/actions/student";
import { courseListSelector } from "../../store/selectors/courses";
import { studentListSelector } from "../../store/selectors/students";
import {
    RemoveCircle as RemoveCircleIcon,
    AddCircle as AddCircleOutlineIcon,
} from '@mui/icons-material';
import { Alert } from '@mui/lab';
import { enrollmentsUrl } from "../../constants/servicesUrls";
import httpClient from "../../services/httpClient";

export const EnrollmentsCreate = () => {

    const [student, setStudent] = useState('');
    const [course, setCourse] = useState('');
    const [fecha, setFecha] = useState<Date | null>(new Date());
    const [orders, setOrders] = useState<any[]>([]);

    const studentList = useSelector(studentListSelector);
    const courseList = useSelector(courseListSelector);

    const dispatch = useDispatch();

    console.log(orders)

    useEffect(() => {
        if (!studentList) dispatch(StudentsActions.getStudents());
        if (!courseList) dispatch(CoursesActions.getCourses());
    }, []);

    const onAddOrder = () => {
        function findCourseName() {
            const objCourse = courseList?.find((c) => c.id === course);
            return objCourse?.nombre;
        }

        const existInCourse = orders.find((x) => x.id === course);
        const newOrderArray = [...orders];

        if (existInCourse) {
            setCourse('');
        } else {
            const order = {
                id: course,
                nombre: findCourseName(),
            };
            newOrderArray.push(order);
        }

        setOrders(newOrderArray);
    };


    const onNewInvoice = async () => {
        function transformOrders() {
            return orders.map((c) => {
                return {
                    id:  c.id ,
                };
            });
        }
        function formatDate(){
            let tzoffset = (new Date()).getTimezoneOffset() * 60000;
            if (fecha != null) {
                let localISOTime = (new Date(fecha.getTime() - tzoffset)).toISOString();
                console.log(localISOTime)
                return localISOTime;
            }
        }
        try {
            const payload = {
                estado: true,
                fechaMatricula: formatDate(),
                estudiante: {
                    id: student,
                },
                cursos: transformOrders(),
            };
            console.log(payload);
            await httpClient.post(enrollmentsUrl, payload);
            setStudent('');
            setCourse('');
            setFecha(new Date());
            setOrders([]);
        } catch (error) {
            <Alert severity="success">Error</Alert>
            console.log(error);
        }
    };

    const onSubtract = (order: any) => {
        const newOrderArray = [...orders];
        const orderDuplicate = { ...order };

        const index = newOrderArray.findIndex(
            (p) => p.id === orderDuplicate.id
        );

        newOrderArray.splice(index, 1);

        setOrders(newOrderArray);
    };

    const renderOrders = () => {
        return (
            <div className="Course">
                <div className="tableOrders__head">
                    <div className="tableOrders__head--item">Course</div>
                    <div className="tableOrders__head--item">Action</div>
                </div>
                <div className="tableOrders__body">
                    {orders.map((order) => (
                        <div className="tableOrders__body--row" key={order.id}>
                            <div className="tableOrders__body--col">
                                {order.nombre}
                            </div>
                            <div className="tableOrders__body--col">
                                <IconButton
                                    color="inherit"
                                    onClick={() => onSubtract(order)}
                                >
                                    <RemoveCircleIcon />
                                </IconButton>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    };

    const handleSelectStudent = (event: any) => {
        setStudent(event.target.value);
        setCourse('');
        setOrders([]);
    };

    const handleSelectCourse = (event: any) => {
        setCourse(event.target.value);
    };

    return (
        <Layout>
            <Grid container spacing={4}>
                <Grid item xs={4}>
                    <label>Student</label>
                    <Select
                        id="student"
                        style={{ width: '100%' }}
                        value={student}
                        onChange={handleSelectStudent}
                    >
                        {studentList &&
                            studentList.map((student) => (
                                <MenuItem value={student.id} key={student.id}>
                                    {student.nombres} {student.apellidos}
                                </MenuItem>
                            ))}
                    </Select>
                </Grid>
                <Grid item xs={4}>
                    <label>Course</label>
                    <Select
                        id="course"
                        style={{ width: '100%' }}
                        value={course}
                        onChange={handleSelectCourse}
                    >
                        {courseList &&
                            courseList.map((course) => (
                                <MenuItem key={course.id} value={course.id}>
                                    {course.nombre}
                                </MenuItem>
                            ))}
                    </Select>
                </Grid>
                <Grid item xs={4}>
                    <IconButton color="inherit" onClick={onAddOrder}>
                        <AddCircleOutlineIcon />
                    </IconButton>
                </Grid>
            </Grid>
            <br />
            <br />
            <Grid container spacing={4}>
                <Grid item xs={12}>
                    <Paper elevation={3}>
                        <div style={{ padding: '15px' }}>
                            {orders.length === 0 && (
                                <div>
                                    You haven't add any plate to your order yet
                                </div>
                            )}
                            {orders.length > 0 && renderOrders()}
                        </div>
                    </Paper>
                </Grid>
            </Grid>
            <br />
            <br />
            <Grid container spacing={4}>
                <Grid item xs={12}>
                    <DatePicker
                        value={fecha}
                        onChange={(date: Date | null) => setFecha(date)}
                        renderInput={(props) => (
                            <TextField {...props} />
                        )}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={4}>
                <Grid item xs={12}>
                    <br />
                    <br />
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={onNewInvoice}
                    >
                        Create Invoice
                    </Button>
                </Grid>
            </Grid>
        </Layout>
    )
};

export default EnrollmentsCreate;