import {
    Button,
    CircularProgress,
    Divider,
    Grid,
    IconButton,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    MenuItem,
    Select,
    Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Layout from "../../components/Layout";
import { clearEnrollmentsByUser, getEnrollmentsByUser, getEnrollmentsList } from "../../store/actions/enrollments";
import { StudentsActions } from "../../store/actions/student";
import { enrollmentStateSelector } from "../../store/selectors/enrollments";
import { studentListSelector } from "../../store/selectors/students";
import { Visibility as VisibilityIcon } from '@mui/icons-material';

const Enrollments = () => {
    const [idSelected, setIdSelected] = useState('');

    const state = useSelector(enrollmentStateSelector);
    const studentList = useSelector(studentListSelector);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleChange = (event: any) => {
        setIdSelected(event.target.value);
    };

    const onSearchByUser = async () => {
        console.log('click');
        dispatch(getEnrollmentsByUser(idSelected));
    };

    const onClearFilter = () => {
        setIdSelected('');
        dispatch(clearEnrollmentsByUser());
    };

    const onDetail = (data: any) => {
        navigate(`/enrollments/${data.id}`);
    };

    const fetchEnrollments = () => {
        dispatch(getEnrollmentsList());
    };

    useEffect(() => {
        if (!studentList) {
            dispatch(StudentsActions.getStudents());
        }
        fetchEnrollments();
    }, []);

    const renderListStudents = () => {
        console.log(state.list.data)
        return state.list.data.map((data: any, i: any) => (
            <ListItem key={i}>
                <ListItemText primary={data.fechaMatricula.substring(0, 10)} />
                <ListItemSecondaryAction>
                    <IconButton
                        edge="end"
                        aria-label="edit"
                        onClick={() => onDetail(data)}
                    >
                        <VisibilityIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        ));
    };

    const renderListStudentsByUser = () => {
        if (state.filterList.data.length === 0) {
            return <div>There are no enrollments for this Students</div>;
        }
        return state.filterList.data.map((data: any, i: any) => (
            <ListItem key={i}>
                <ListItemText primary={data.fechaMatricula.substring(0, 10)} />
                <ListItemSecondaryAction>
                    <IconButton
                        edge="end"
                        aria-label="edit"
                        onClick={() => onDetail(data)}
                    >
                        <VisibilityIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        ));
    };

    return (
        <Layout>
            <Grid item xs={12} md={12}>
                <Typography variant="h4" gutterBottom>
                    Enrollment List
                </Typography>
                <div>
                    <Grid container spacing={4}>
                        <Grid item xs={6}>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                style={{ width: '100%' }}
                                value={idSelected}
                                onChange={handleChange}
                            >
                                {studentList &&
                                    studentList.map((student, i) => (
                                        <MenuItem key={i} value={student.id}>
                                            {student.nombres} {student.apellidos}
                                        </MenuItem>
                                    ))}
                            </Select>
                        </Grid>
                        <Grid item>
                            <Button
                                onClick={onSearchByUser}
                                variant="contained"
                                color="primary"
                            >
                                Search
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button onClick={onClearFilter} variant="contained">
                                Clean
                            </Button>
                        </Grid>
                    </Grid>
                </div>
                <br />
                <br />
                <Divider />
                <br />
                <br />
                <div>
                    <List dense={false}>
                        {(state.list.loading || state.filterList.loading) && (
                            <CircularProgress color="inherit" />
                        )}
                        {state.list.data &&
                            state.filterList.data === null &&
                            renderListStudents()}
                        {state.filterList.data !== null &&
                            renderListStudentsByUser()}
                    </List>
                </div>
            </Grid>
        </Layout>
    )
};

export default Enrollments;