export const loginUrl = '/login';
export const coursesUrl = '/cursos';
export const studentsUrl = '/estudiantes';
export const  enrollmentsUrl = '/matriculas';
export const  enrollmentsByUserUrl = '/matriculas/buscar';
