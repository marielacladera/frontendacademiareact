export const baseRoute = '/';
export const loginRoute = '/login';
export const studentsRoute = '/students';
export const coursesRoute = '/courses';
export const enrollmentsRoute = '/enrollments'; 